#pragma once
#include <string>
#include <vector>
class Animal
{
public:
	Animal();
	virtual ~Animal();

	void Eat();

protected:
	int x;
	int y;
	std::string name;

};

