#pragma once
#include "Animal.h"
#include "MovableAnimal.h"
class Dog :
	public MovableAnimal
{
public:
	Dog();
	~Dog();
private:
	std::vector<std::string> learnedCommands;
};

