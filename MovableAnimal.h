#pragma once
#include "Animal.h"
class MovableAnimal : public Animal
{
public:
	MovableAnimal();
	~MovableAnimal();

	void Move();
};

